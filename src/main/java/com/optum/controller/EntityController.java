package com.optum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.optum.enums.ResponseType;
import com.optum.model.EntityDto;
import com.optum.model.ResponseEntity;
import com.optum.service.EntityService;

@RestController
public class EntityController {

	@Autowired
	private EntityService entityService;
	
	
	@RequestMapping("/test")
	public String test() {
		return "Hello";
	}

	@RequestMapping( value = "/addentity", method = RequestMethod.POST)
	public ResponseEntity getCatalog(EntityDto dto, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			System.out.println(bindingResult.getAllErrors());
		}
		ResponseEntity r = new ResponseEntity();
		if (dto.getEntityType() != null) {
			EntityDto result=entityService.saveEntity(dto);
			
			r.setEntry(result);
			r.setResponseType(ResponseType.Success);
			r.setMessage("Entity Save Successful");
		} else {
			r.setResponseType(ResponseType.Failure);
			r.setMessage("Entity Save Failure");
		}
		return r;
	}

}
