package com.optum.service;

import com.optum.model.EntityDto;

public interface EntityService {

	EntityDto saveEntity(EntityDto edto);

}
