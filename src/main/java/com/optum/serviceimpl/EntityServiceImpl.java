package com.optum.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.stereotype.Service;

import com.optum.Repo.EntityRepo;
import com.optum.model.EntityDto;
import com.optum.service.EntityService;

@Service
public class EntityServiceImpl implements EntityService {

	@Autowired
	private EntityRepo entityRepo;

	@Override
	public EntityDto saveEntity(EntityDto edto) {
		EntityDto dto =entityRepo.saveEntity(edto);
		return dto;
	}

}
