package com.optum.Repo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.optum.model.EntityDto;

@Component
public class EntityRepo {
	public static List<EntityDto> dto = new ArrayList<EntityDto>();

	public EntityDto saveEntity(EntityDto edto) {
		dto.add(edto);
		return edto;
	}
}
