package com.optum.model;

import com.optum.enums.EntityType;

public class EntityDto {
	
	private EntityType entityType;
	
	private String name;
	
	private String alternateiveName;
	
	private String TIN;
	
	private String startDate;
	
	private String endDate;

	public EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlternateiveName() {
		return alternateiveName;
	}

	public void setAlternateiveName(String alternateiveName) {
		this.alternateiveName = alternateiveName;
	}

	public String getTIN() {
		return TIN;
	}

	public void setTIN(String tIN) {
		TIN = tIN;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	

}
