package com.optum.model;

import com.optum.enums.ResponseType;

public class ResponseEntity {
	
	private ResponseType responseType;
	
	private String message;
	
	private Object entry;
	

	public Object getEntry() {
		return entry;
	}

	public void setEntry(Object entry) {
		this.entry = entry;
	}

	public ResponseType getResponseType() {
		return responseType;
	}

	public void setResponseType(ResponseType responseType) {
		this.responseType = responseType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
